package com.devcamp.s50.task56c70.animalapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c70.animalapi.models.Animal;
import com.devcamp.s50.task56c70.animalapi.models.Cat;
import com.devcamp.s50.task56c70.animalapi.models.Dog;

@Service
public class AnimalService {
     //khởi tạo đối tượng cat
     Cat cat1 = new Cat("Miu");
     Cat cat2 = new Cat("Myo");
     Cat cat3 = new Cat("Min");

     //khởi tạo đối tượng dog 
     Dog dog1 = new Dog("Muc");
     Dog dog2 = new Dog("Den");
     Dog dog3 = new Dog("Xu");

     //tạo ArrayList Animal
     public ArrayList<Animal> allAnimals(){
          ArrayList<Animal> animals = new ArrayList<>();

          animals.add(cat1);
          animals.add(cat2);
          animals.add(cat3);

          animals.add(dog1);
          animals.add(dog2);
          animals.add(dog3);

          return animals;
          
     }
     


}

package com.devcamp.s50.task56c70.animalapi.models;

public class Dog extends Mammal{
     public Dog(String name) {
        super(name);

    }

    @Override
    public String toString() {
        return "Dog [" + super.toString() + "]";
    }

    public void greets() {
        System.out.println("Woof");
    }

    public void greets(Dog dog) {
        System.out.println("Wooooof");
    }
}

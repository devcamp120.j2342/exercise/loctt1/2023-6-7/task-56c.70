package com.devcamp.s50.task56c70.animalapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c70.animalapi.models.Cat;
import com.devcamp.s50.task56c70.animalapi.models.Dog;
import com.devcamp.s50.task56c70.animalapi.services.CatService;
import com.devcamp.s50.task56c70.animalapi.services.DogService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AnimalController {
     @Autowired
     private CatService catService;

     @Autowired
     private DogService dogService;

     @GetMapping("/cats")
     public ArrayList<Cat> catList(){
          ArrayList<Cat> cats = catService.allCats();
          return cats;
     }

     @GetMapping("/dogs")
     public ArrayList<Dog> dogList(){
          ArrayList<Dog> dogs = dogService.allDogs();
          return dogs;
     }
}

package com.devcamp.s50.task56c70.animalapi.models;

public class Cat extends Mammal{
     public Cat(String name) {
        super(name);

    }

    @Override
    public String toString() {
        return "Cat [" + super.toString() + "]";
    }

    public void greets() {
        System.out.println("Meow");
    }
}

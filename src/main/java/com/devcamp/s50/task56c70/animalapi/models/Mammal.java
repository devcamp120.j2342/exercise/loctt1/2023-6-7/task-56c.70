package com.devcamp.s50.task56c70.animalapi.models;

public class Mammal extends Animal{
     public Mammal(String name) {
        super(name);

    }

    @Override
    public String toString() {
        return "Mammal [" + super.toString() + "]";
    }

}
